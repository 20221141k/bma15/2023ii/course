/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package pe.edu.uni.jrodriguezb.exceptions;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Exception !!!");
        int[] array = {10, 20, 30, 40, 50, 60};
        int i = 0;
        /* if (i < array.length) {
            System.out.println(array[i]);
        } else {
            System.out.println("Fuera de los límites");
        }*/
        //try-catch usar cuando es inevitable usar excepciones
        try {// en try le metes lo menos posible
//abrir la base de datos
// el error que puede cometer el usuario
            //el try hace que el codigo que este en el contexto del try, si genera un error de ingreso de dato, entonces ese error que se detecta es resuelto en el contexto del catch
            //intenta hacer eso, y si no puedes lo atrapas en el catch el error
            System.out.println(array[i]); // si le pongo 6, ya no ejecuta el demas contexto
            // puede existir una o mas excepciones
//           int j=(10/i);
            Function(i);// lo de abajo ya no se ejecutara dado que aca hay un error que se lanza, asi que solo usar cosas 
            // hay una manera de completar lo que no se ejecuto, es con el finally
            System.out.println("hola"); // para que esto se ejecute, debes ponerlo en finally
       //cerrar la base de datos
       //el finally se ejecuta si un catch es activado
        } catch (ArrayIndexOutOfBoundsException e) {// catch (Exception e){
//la excepcion que puede ser utilizada
            //ArrayIndexOutOfBoundsException cuando el i sobrepasa al tamaño del arreglo
            //puede haber n tipos de excepciones
            //puede haber excepciones personalizadas
            System.out.println("Fuera de los límites");
            System.out.println(e); //imprime el error que se almaceno en la variable e
        } catch (ArithmeticException e) {// puede haber n posibilidades que se manejen dentro del mismo try
            System.out.println("División entre cero");
            System.out.println(e);
        }// ctrl+shift+c se comenta toda la linea en la que estes, con el shift+arriba o abajo seleccionas 
        catch (Exception e) {    //default
            //si el primer catch catcha algo, los demas catchs no se operan, si el default va arriba entonces los demas catchas ya no son utilizados
            // cuando en la funcion o en el contexto del try el error que ha sido lanzado no puede ser catchado por ninguno de los chatchs, hay un default
            // sale exception cuando el default va primero,cuando va mas arriba de los demas
            //sale error dado que no deberia estar ahi el default cuando vuelvo a catchar el error
           
        }finally{// hay una manera de completar lo que no se ejecuto, es con el finally
            //se ejecuta luego del catch
            System.out.println("hola");
            System.out.println("Cierra la base de datos");
//            System.out.println(10 / i); sale una excepcion 
        }
    }

    public static void Function(int i) throws ArithmeticException { //throws es lanzar,lancemos alguna excepcion que yo tenga definida o que java tenga definida
        //si aca adentro hay alguna excepcion que tenga que ver con arithmetic exception, la funcion lo lanza
        // el desarrollador define que excepcion va a lanzar
        int j = (10 / i);
        System.out.println("hola"); // catcha la excepion y lo lanza y no hace lo demás
        // lo detiene donde se equivoca
    }
}
