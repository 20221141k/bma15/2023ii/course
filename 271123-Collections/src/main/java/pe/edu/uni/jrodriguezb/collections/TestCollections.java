/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.jrodriguezb.collections;

import java.util.ArrayList;
import java.util.Collection;//import collection llama  atodas las interfaces que estan ahi

/**
 *
 * @author PROFESOR
 */
public class TestCollections {

    
    public static void main(String[] args) {
        System.out.println("Hello World!");
        String[] cities = {"Arequipa", "Lima", "Piura", "Oxapampa", "Callao", "Junin", "Iquitos"};
        ArrayList<String> collection1 = new ArrayList<>();//array list no tiene un tamaño fijo, 

        collection1.add(cities[0]);
        collection1.add(cities[1]);
        collection1.add(cities[2]);
        collection1.add(cities[3]);
        System.out.println("Una lista de ciudades: " + collection1);
        System.out.println("La ciudad " + cities[3] + " está presente en la lista?: " + collection1.contains(cities[3]));
        int i=0;
        if (collection1.contains(cities[i])) {
            System.out.println("La ciudad " + cities[i] + " está contenida!");
        } else {
            System.out.println("La ciudad " + cities[i] + " no está contenida!");

        }
        i=3;
        if (collection1.remove(cities[i])) {// si no le encuentra, devuelve un false
            System.out.println(cities[i]+" ha sido removido");
        }else{
            System.out.println(cities[i]+" no ha sido removido");
        }
        System.out.println(collection1.size()+ " ciudades presentes!");
        //en listas:
        //no usamos el mismo tipo de varialbe, usamos una interfaz, nosotros podremos usar lo meotodos de collection, cosa que antes no podiamos
        Collection</*que tipo de dato va a manejar */ String> collection2=new ArrayList<>(); //lo anterior fue hecho para demostrar que si es posible, pero ahora lo hacemos con una interfaz llamada collection, mientras que arraylist es la clase en concreto
        collection2.add(cities[4]);
        collection2.add(cities[5]);
        collection2.add(cities[6]);
        collection2.add(cities[1]);
        System.out.println("Lista de ciudades en collection 2: "+collection2);
        ArrayList<String> c1;
        c1= (ArrayList<String>) collection1.clone();//llamamos a un clone de collection 1, me trae el mismo conjunto de objetos, me lo copia identico pero no apuntan a lo mismo
        // lo de arriba es un casteo ya que el clone no sabe que objeto recibe, asi que se castea a un arraylist
        c1.addAll(collection2);
        
        System.out.println("Ciudades en collection1 o collection2: "+c1);
        c1= (ArrayList<String>) collection1.clone();
        c1.retainAll(collection2);// la interseccion entre collection1 y collection2
        System.out.println("Ciudades en collection1 y collection2: "+c1);
        c1= (ArrayList<String>) collection1.clone();
        c1.removeAll(collection2); //remueve todo lo de collection 2
        System.out.println("Ciudades en collection1 y collection2: "+c1);
        System.out.println("");
    }
}
