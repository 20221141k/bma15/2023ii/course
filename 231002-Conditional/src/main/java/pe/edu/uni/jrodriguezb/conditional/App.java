/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.jrodriguezb.conditional;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Conditional statments!");
        int i = 35;
        if (30 < i) {
            System.out.println("el número es mayor a 30");
        }

        if (i % 2 == 1) {
            System.out.println("Es impar");
        }
        if (i % 2 == 0) {
            System.out.println("Es par");
        }
        

        System.out.println("-----------------");

        int j = 31;
        if (30 < j) {
            System.out.println("el número es mayor a 30");
        } else {

            if (j % 2 == 1) {
                System.out.println("Es impar");
            } else {
                if (j % 2 == 0) {
                    System.out.println("Es par");
                }
            } 
// alt + shift+f el codigo se autoacomoda

           
        }
          System.out.println("+-----------------");
        
          /*int k = 34;
        if (30 < k) {
            System.out.println("el número es mayor a 30");
            return;
        }

        if (k % 2 == 1) {
            System.out.println("Es impar");
            return;
        }
        if (k % 2 == 0) {
            System.out.println("Es par");
            return; //salimos de la funcion y ya no hace nada, ya que le devuelve a una funcion void un return vacio
            //buenas practicas indican que el codigo debe estar por encima del return y no debe haber nada
        }
para q la  funcion no se acabe con el return        */
        
        int m=12;
        switch(m%2){ //probar el char en el switch
            case 0: 
                System.out.println("Es par");
                break; //se evalua y se acaba el switch
            case 1:
                System.out.println("Es impar");
                break;
            default:
                System.out.println("Cualquier otro numero");
                //break;no se le pone dado que es el ultimo
        }
        
    }
}
