/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.jrodriguezb.arrays;

/**
 *
 * @author User
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Arrays!");
        int arraySize = 5;
        double[] styleJavaArray; //Dos formas de declarar arrays
        double styleCppArray[];

        styleJavaArray = new double[arraySize]; //para ponerle tamaño al array
        styleCppArray = new double[arraySize];
        //[0.0,0.0,0.0,0.0,0.0]
        styleJavaArray[0] = 5;
        styleJavaArray[1] = 4;
        styleJavaArray[2] = 3;
        styleJavaArray[3] = 2;
        styleJavaArray[4] = 1;

        System.out.println("styleJavaArray: " + styleJavaArray[2]);
        System.out.println("Visualizando: ");
        for (int i = 0; i < styleJavaArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + styleJavaArray[i]);
        }
        System.out.println("Definiendo");
        for (int i = 0; i < styleJavaArray.length; i++) {
            styleJavaArray[i] = i;
        }
        System.out.println("Visualizando");
        for (int i = 0; i < arraySize; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + styleJavaArray[i]);
        }
        //Inicializando forma 2
        double[] myArra = {1.9, 2.9, 3.4, 3.5, 7, 2};
        System.out.println("Visualizando:");
        for (int i = 0; i < myArra.length; i++) {
            System.out.println("myArray[" + i + "] = " + myArra[i]);
        }
        //ingresar valores
        /*System.out.println("Escribir nuevos valores");
        java.util.Scanner input=new java.util.Scanner(System.in); //////////////////////////////
        System.out.println("Ingrese "+myArra.length+" valores:");
        for (int i=0;i<myArra.length;i++){
            myArra[i]=input.nextDouble(); //////////////////////////////////////////// escribir elementos
        }
        System.out.println("Visualizandoo");
        for(int i=0;i<myArra.length;i++)
        {System.out.println("myArray["+i+"]= "+myArra[i]);
        }*/
        //Inicializando valores aleatorios
        for (int i = 0; i < myArra.length; i++) {
            myArra[i] = (int) (Math.random() * 100);
        }
        System.out.println("Visualizandoo");
        for (int i = 0; i < myArra.length; i++) {
            System.out.println("myArray[" + i + "]= " + myArra[i]);
        }
        System.out.println("random: " + (int) ((Math.random() * 9) + 1)); //formula valores entre x e y (x-y+1)+y
        double[] gaa;
        gaa = new double[9];
        int k = 0;
        for (int i = 0; i < 9; i++) {
            gaa[i] = (int) ((Math.random() * 9) + 1);
            for (int j = 0; j < 9; j++) {
                if (gaa[j] == gaa[i]) {
                    if (i == j) {
                    } else {
                        k = 1;
                    }

                } else {
                }
            }
            if (k == 1) {
                i--;
            }
            k = 0;
        }
        for (int i = 0; i < 9; i++) {
            System.out.println("gaa[" + i + "]= " + gaa[i]);
        }
        //suma
        double total = 0;
        for (int i = 0; i < gaa.length; i++) {
            total += gaa[i];
        }
        System.out.println("total: " + total);
        //Encontrar el elemento mas grande
        double max = myArra[0];
        int indexOfMax = 0;
        for (int i = 0; i < myArra.length; i++) {
            if (myArra[i] > max) {
                max = myArra[i];
                indexOfMax = i;
            }
        }
        System.out.println("max: " + max);
        System.out.println("Index of max: " + indexOfMax);
        //meezclar elementos
        for (int i = 0; i < myArra.length; i++) {
            //generamos un index aleatorio
            int j = (int) (Math.random() * myArra.length);
            //  System.out.println("j: "+j);
            double temp = myArra[i];
            myArra[i] = myArra[j];
            myArra[j] = temp;
        }
        System.out.println("Visualizandoo");
        for (int i = 0; i < myArra.length; i++) {
            System.out.println("myArray[" + i + "]= " + myArra[i]);
        }
        //shifting a la izquierda
        //[10,20,30,40,50]
        //[20,30,40,50,10]
        double temp = myArra[0]; //temp tiene el primer elemento
        for (int i = 1; i < myArra.length; i++) {
            myArra[i - 1] = myArra[i];
        }
        myArra[myArra.length - 1] = temp;
        System.out.println("Visualizandoo");
        for (int i = 0; i < myArra.length; i++) {
            System.out.println("myArray[" + i + "]= " + myArra[i]);
        }
        //
        String[] months={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre"};
        java.util.Scanner hola= new java.util.Scanner(System.in);
        System.out.println("Igresa el numero del mes (1 al 12): ");
        int holaxd =(int) hola.nextDouble();
        System.out.println("El mes es: "+(months[holaxd-1]));
    }
}
