/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.arrays;

/**
 *
 * @author User
 */
public class ArrayLoops {

    public static void main(String[] args) {
        System.out.println("Array Loops !!!");
        for(int i=0;i<args.length;i++){
            System.out.println(args[i]);
        }
        System.out.println("Ingrese los valores a procesar");
        java.util.Scanner input = new java.util.Scanner(System.in); // el java scanner no puede entrar en el do while
        int item = 1; // cuando yo declaro un entero, siempre se inicializa con un cero por defecto
        int[] numeros;
        numeros = new int[1000];
        int counter = 0; //presionar ctrl r para cambiar de golpe todas las variables
        do {
            item = input.nextInt();
            if (item != 0) {
                numeros[counter] = item;
                counter++;
                System.out.println("item: " + item);
            }

        } while (item != 0);
        System.out.println("counter: "+counter);
        for (int i=0;i<counter;i++){
            System.out.print(numeros[i]+" ");
        }
    }

}
