/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;

/**
 *
 * @author User
 */
public class StackOfIntegers {

    private int[] elements;
    private int size;
    private static final int DEFAULT_CAPACITY = 16; //LAS VARIABLES ESTATICAS  FINAL PUEDEN SER PUBLICAS, SI NO FUESE CONSTANTE ENTOONCES PUEDE MODIFICARSE Y DEBE SER PRIVADA,

    public StackOfIntegers() {
      //  elements = new int[DEFAULT_CAPACITY];
        this(DEFAULT_CAPACITY);
    }

    public StackOfIntegers(int capacity) {
        elements = new int[capacity];
    }
    public boolean empty(){
        return(size==0);
    }
    public int peek(){
       return elements[size-1];
    }
    public int getSize(){
       return size; 
    }
    /**
     * solo agrega enteros hasta la capacidad definida
     * @param value 
     */
    public void push(int value){
       
        if (elements.length>size){
            elements[size++]=value;
         //   size++;
        }
    }
    public int pop(){
        return elements[--size]; //solo lectura, falta implementar eliminacion
    }
}
