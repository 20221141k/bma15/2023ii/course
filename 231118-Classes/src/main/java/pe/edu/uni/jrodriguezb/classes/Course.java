/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;

/**
 *
 * @author User
 */
public class Course {
    private String courseName;
    private String[] students;
    private int numberOfStudents;
    private final int MAX_STUDENTS = 100; // final indica constante
    public Course(String courseName){
        this.courseName=courseName;
        students=new String[MAX_STUDENTS];
    }
    public String getCourseName(){
        return courseName;
    }
    
    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String[] getStudents() {
        return students;
    }
    void addStudent(String student){
        students[numberOfStudents]=student;
        numberOfStudents++;
    }
    void dropStudents(String student){
        // HAZLO TU 
    }
    
}
