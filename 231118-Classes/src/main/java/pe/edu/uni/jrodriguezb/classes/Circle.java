/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class Circle extends GeometricObject {
    private double radius;
    private  final double DEFAULT_RADIUS =1;
    public Circle(){
         super();
         this.radius=DEFAULT_RADIUS;
    }
    public Circle(double radius){
        super();
        this.radius=radius;
    }
    public Circle(double radius, String color, boolean filled){
        super(color,filled);
        this.radius=radius;
        
    }
    public double getRadius(){
        return radius;
    }
    public void setRadius(double radius){
        this.radius=radius;
    }
    public double getArea(){
        return Math.PI*radius*radius;
    }
    public double getPerimeter(){
        return 2*Math.PI*radius;       
    }
    public double getDiameter(){
        return 2*radius;
    }
    public void printCircle(){
        System.out.println("Creación " + getDateCreated()+", radio: "+this.radius);
    }
}
