/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class TestCircleRectangle {
    public static void main(String[] args) {
        System.out.println("Circle!!!");
        Circle circle=new Circle(1,"rojo",false);
        System.out.println("El circulo: "+circle.toString());
        circle.printCircle();
        System.out.println("El radio: "+circle.getRadius());
        System.out.println("El diametro: "+circle.getDiameter());
        System.out.println("El area: "+circle.getArea());
        System.out.println("El perimetro: "+circle.getPerimeter());
        String newColor="red";
        circle.setColor(newColor);
        System.out.println(circle.getColor());
        
        System.out.println("Rectangle!!!");
        Rectangle rectangle=new Rectangle(2,4,"blue",true);
        System.out.println("El rectangulo: "+rectangle.toString());
        System.out.println("De ancho: "+rectangle.getWidth());
        System.out.println("De alto: "+rectangle.getHeight());
        System.out.println("perimetro: "+rectangle.getPerimeter());
        System.out.println("area: "+rectangle.getArea());
        rectangle.setFilled(!rectangle.isFilled());
        rectangle.setColor(circle.getColor());
        System.out.println("Ahora es el rectangulo: "+ rectangle.toString());
        
    }
}
