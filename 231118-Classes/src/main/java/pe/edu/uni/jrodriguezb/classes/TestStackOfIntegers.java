/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;

/**
 *
 * @author User
 */
public class TestStackOfIntegers {
     public static void main(String[] args) {
        StackOfIntegers stack=new StackOfIntegers();
        for (int i = 0; i < 18; i++) { // por mas que le ponga mas, lo maximo es la capacidad de d16
            stack.push(i);
        }
        while (!stack.empty()){
            System.out.println(stack.pop()+" ");
        }
    }
}
