package pe.edu.uni.jrodriguezb.linkedlists;

import java.util.Collections;
import java.util.LinkedList;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {
    
    private LinkedList<Integer> list=new LinkedList<>();
    private Label lbMessage=new Label("Ingrese un número");
    private TextField tfNumber=new TextField();
    private TextArea taNumbers= new TextArea();
    private Button btSort =new Button("Ordenar");
    private Button btShuffle =new Button("Aleatorio");
    private Button btReverse =new Button("Invertir");
    
    @Override
    public void start(Stage stage) {
      //  var javaVersion = SystemInfo.javaVersion();
      //  var javafxVersion = SystemInfo.javafxVersion();

      //  var label = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
    //    var scene = new Scene(new StackPane(label), 640, 480);
        HBox hBox=new HBox(10);//horizontal box    
        hBox.getChildren().addAll(lbMessage,tfNumber); //en la parte izquierda el message y la parte derecha el number//a la bandeja de la caja horizontal le vas a agregar lo que te dare
        hBox.setAlignment(Pos.CENTER);//que lo centre
        HBox hBoxForButtons =new HBox(10);
        hBoxForButtons.getChildren().addAll(btSort,btShuffle,btReverse);
        hBoxForButtons.setAlignment(Pos.CENTER);
        
        BorderPane borderPane=new BorderPane();    
        borderPane.setTop(hBox);//en la parte superior colocara el hbox
        borderPane.setCenter(new ScrollPane(taNumbers));//en la parte central del pane coloca la caja de texto
        borderPane.setBottom(hBoxForButtons);
        var scene = new Scene(borderPane, 400, 220);
        stage.setScene(scene);
        stage.show();
        tfNumber.setOnAction(e-> {
        
            System.out.println("Ingresar número");
            String sNumber=tfNumber.getText();
            if(!list.contains(Integer.valueOf(sNumber))){
                list.add(Integer.valueOf(sNumber));
                taNumbers.appendText(sNumber+" ");
            }
            tfNumber.setText("");
        });
        btSort.setOnAction(e -> {// entre llaves esta ambito//el evento que entre por el landa que se capture, se resuelve en las llaves
            System.out.println("ordenar!");
        Collections.sort(list);
        display();
        });
        btShuffle.setOnAction(e->{
        
            System.out.println("aleatorio!");
        Collections.shuffle(list);
        display();
        });
        btReverse.setOnAction(e->{
        
            System.out.println("reversa!");
        Collections.reverse(list);
        display();
        });
    }
    private void display(){
    taNumbers.setText(null);
        for (Integer integer : list) {
            taNumbers.appendText(integer+" ");
        }
    }
    public static void main(String[] args) {
        launch();
    }

}