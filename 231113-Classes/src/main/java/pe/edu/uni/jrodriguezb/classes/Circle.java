/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class Circle {// el constructor siempre tiene el mismo nombre que la clase !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    double radious; // si no hay un menos o un mas en el diagrama de clase, entonces no es necesario poner private o public, si no se pone nada, lo que hace java es que sea privado, por defecto

    Circle() {   //constructor
        this.radious = 1; //hacemos referencia al radious de esta misma clase, osea a la clase circle   
    }

    Circle(double newRadious) {//se ejecuta cuando se instancia un objeto; solo se ejecuta en la instanciacion
        this.radious = newRadious;
    }

    double getArea() {
        //  return Math.PI*radious*radious; //tambien sirve
        return Math.PI * this.radious * this.radious; // Math.PI es el pi=3.14!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

    double getPerimeter() {
        return 2 * Math.PI * this.radious;
    }

    void setRadious(double radious) {//cuando no se instancia un objeto y necesitamos modificar el valor
        this.radious = radious;//el this.radious es de la clase
        //radious=radious;no hace nada, pero genera un conflicto
    }
    //clic derecho, insert code,tostring[] para poner un solo get,  trae el valor de todos los campos, variables
    //overload funcion sobre cargada, cambiamos los parametros siendo la misma funcion  f(int), f(double),f(string),...
    //el papa de todas las clases es la clase object

    @Override // no cambia la firma de la funcion, to string[], y devuelve string, asi esta definida en el papá, pero la estamos re definiendo
    public String toString() {
        return "Circle{" + "radious=" + radious + '}'; // esta linea solo depende de la cantidad de campos que tenga la classe
    }

}
