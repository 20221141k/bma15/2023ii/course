/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;
//el import se agrega cuando una clase no  esta en el mismo package , y se invoca al otro paquete

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class TestCircle {//cambiamos a TestCircle el App al cambiar en projectfiles , pom

    public static void main(String[] args) {
        System.out.println("Test Circle!");
        Circle circle1;//Creamos una referencia de la clase Circle
        //una referencia puede ser ubicada por un puntero, aca no hay punteros
        //aca las referencias pueden ser manejadas
        circle1 = new Circle(); //el nuevo objeto es circle 1 // creamos un objeto tipo Circle, y es guardada en mi referencia circle1
        System.out.println(circle1.toString()); // compila sin la necesidad de poner el tostring[] en la clase circle, porqie el hijo no sabe que campo esta usando el papa
        System.out.println("perimeter 1: " + circle1.getPerimeter());
        System.out.println("area 1: " + circle1.getArea());
        double radious2 = 25;
        Circle circle2;
        circle2 = new Circle(radious2);
        System.out.println(circle2.toString());
        System.out.println("perimeter 2: " + circle2.getPerimeter());
        System.out.println("area 2: " + circle2.getArea());

        //si en caso yo voy a conocer todavia despues el valor del radious3, entonces se inicializa asi:
        Circle circle3 = new Circle(); // se inicializa el radious con 1
        double radious3 = 125;
        //actualizamos el valor de radious3;
        circle3.setRadious(radious3);
        System.out.println(circle3.toString());
        System.out.println("perimeter 3: " + circle3.getPerimeter());
        System.out.println("area 3: " + circle3.getArea());
        // podemos crear el objeto : "new Circle()" y hacer new Circle().toString() se crea un objeto anonimo, si le asignamos un circle=new Circle().toString() entonces sabemos que se esta guardando en la referencia circle
   //cada vez que yo hago un new Circle().toString(), es el objeto new Circle() que ejecuta la funcion toString()
   
   //si ponremos new Circle().toString();
   // y tambien  new Circle().toString(); son dos objetos diferentes, que estan guardados en dos lugares distintos, llamados objetos anonimos dado que no conozco el lugar donde estan guardados
   //si ponremos new Circle().toString();
   // y tambien  new Circle(1).toString(); tienen los mismos valores pero no son el mismo objeto.
   
    }
}
