/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class TV {
    int channel;
    int volumeLevel;
    boolean on;
    final int minVolumeLevel=1; // en el final siempre se inicializa
   final int maxVolumeLevel=7;
   final int minChannel=1;
    final int maxChannel=120;
    public TV(){// en caso no exista un constructor declarado, java coloca un constructor default vacio, ya que la clase siempre necesita un constructor
        channel=1;
        volumeLevel=3;
        on=false;
     /*   minVolumeLevel=1;
     maxVolumeLevel=120;
     minChannelLevel=1;
     maxChannelLevel=7; ya no funciona por el final*/
        
    }

    public int getVolumeLevel() {
        return volumeLevel;
    }

    public boolean isOn() {
        return on;
    }
    public void turnOn(){
        this.on=true; //puede no estar el this.
    }
    public void turnOff(){
        this.on=false;
    }
    public void setChannel(int newChannel){
        this.channel=newChannel;
    }
    public void setVolume(int newVolumeLevel){ // los valores deben pasarse como variables, f(25) no es una buena practica
        this.volumeLevel=newVolumeLevel;
    }
    public void channelUp(){
        if (on&&channel<maxChannel){// no es necesario poner on==true
        this.channel++;
        }else if (channel==maxChannel){
            channel=minChannel;
        }    
    }
    public void channelDown(){
        if (on&&minChannel<channel){
            this.channel--;
        }else if(channel==minChannel){
            channel=maxChannel;
        }
        
    }
    public void volumeUp(){
        if(on&&volumeLevel<maxVolumeLevel){
            this.volumeLevel++;
        }
       
    }
    public void volumeDown(){
       if (on&&minVolumeLevel<volumeLevel){
           this.volumeLevel--;
       }
       
    }
    @Override
    public String toString() {
        return "TV{" + "channel=" + channel + ", volumeLevel=" + volumeLevel + ", on=" + on+'}';
    }
}
