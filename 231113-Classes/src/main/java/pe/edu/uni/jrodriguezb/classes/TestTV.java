/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.classes;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class TestTV {
    public static void main(String[] args) {
        TV tv1=new TV();
        tv1.turnOn();
     //   System.out.println(tv1.toString());
        if (tv1.isOn()){
            System.out.println("Encendido!!!");
            int channel=120;
            int volumeLevel=7;
            tv1.setChannel(channel);
            tv1.setVolume(volumeLevel);
            System.out.println(tv1.toString());
            tv1.channelUp();
            System.out.println(tv1.toString());
          
            tv1.volumeUp();
            System.out.println(tv1.getVolumeLevel());
            
        }else{
            System.out.println("Apagado!!!");
        }
    }/*papá(int..., double...)
    entonces hijo(int...,double...)
    */
    //el super() si no lo coloco esta implicito, a que le pertenece al papá 
    //this pertenece al papá
}
//tipos primitivos int, string, float, etc
//tipos de objetos Circle, TV,...