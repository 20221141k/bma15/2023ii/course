/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jrodriguezb.operators;

import java.io.File;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.print("-");
        
        System.out.println("oek xddd ");
        System.out.println("+");
        
        int y1 = 4;
        double x1 = 3 + 2 * --y1;
        System.out.println("x1: " + x1);
        System.out.println( "y1:" + y1);
        //arithmetic operators
        int x2=2*5+3*4-8;
        System.out.println("x2: "+x2);
        int x3=2*((5+3)*4-8);
        System.out.println("x3: "+x3);
        System.out.println(9/3);
        System.out.println(9%3);
        System.out.println(10/3.2);
        System.out.println(10%3.54);
        System.out.println(11.56%3.214);
        
        int x4=1;
        long y4=33;
        System.out.println(x4*y4); //long promueve al int y la salida es long
        System.out.println(x4*y4-0.0);
        System.out.println("bytes "+Integer.SIZE);
        System.out.println("bytes "+Long.SIZE);
        System.out.println("bytes "+Short.SIZE);
        System.out.println("bytes "+Float.SIZE);
        System.out.println("bytes "+Double.SIZE);
        
       
         double x5 = 4.2;
        float y5= 2.1f; //agregar f cuando sea float siempre
        System.out.println(x5+y5);
        
        short x6=10;
        short y6=3;
        System.out.println(x6/y6); //promueve a int //short siempre se promueve a int en una operacion aritmetica
        
        short x7=17;
        float y7=3;
        double z7=38;
        System.out.println(x7*y7/z7);  //short se promueve a int cuando hay una operacion aritmetica, despues se prmueve a float para trabajar con float y luego se promueve con double
              //si hay una operacion aritmetica de por medio, primero se hace la promocion y luego los calculos
         //print no promueve nada, la promocion ocurre solo dentro del parentesis
        boolean x8=false; //tipo primitivo
        //Boolean x9=false; //clase
        System.out.println("x8: "+x8);      
        x8=!x8;
        System.out.println("x8: "+x8); //negacion
        double x9=1.21;
        System.out.println("x9: "+x9);
        x9=-x9;
        System.out.println("x9:  "+x9);
        int counter=0;
        System.out.println("counter: "+ counter);
        System.out.println("counter: "+ ++counter);
        System.out.println("counter: "+ counter);
        System.out.println("counté: "+ counter--);
        System.out.println("counter: "+ counter);
        
        int x = 3;
        int y= ++x * 5/ x-- + --x; //4*5/4+2
        System.out.println("x: "+x);
        System.out.println("y: "+y);
        //int x =1.8;
        //short yo=32767;
        //short yoo=32768;
        //int t= 2147483647;
        //int t1= 2147483648;
        //long trtr= 9223372036854775807L; no compila
        //int f1=1.0
        int a1=(int)10.99999; //el casteo se realiza en la asignacion, no es antes o despues, es cuando le asigno el valor a la variable
        //float a2=(int)10.99999;
        //double a2=(float)10.99999;
        System.out.println("prubea: "+a1);
        short b1 = (short)43546243;
        System.out.println("prueba2: "+b1);
        int c1=(int) 9.5f;
        System.out.println("prueba3: "+c1);
        //long trtrt= (long) 9223372036854775807; o se puede
        long trtrt=  9223372036854775807L;
        System.out.println("queque: "+trtrt);
        
        short a2=10;
        short b2=3;
        //short c2= (short)(a2*b2); compila, el short le dice al q esta al costado(int) que lo convierta  a short. se le llama cast
        int c2= (a2*b2);
        System.out.println("c2: "+c2);
        int a3=2,b3=3;
        //a3=a3*b3; //asignacion simple, no es una buena practica repetir una varible en ambos lados
        a3 *= b3; //asignacion compuesta
        System.out.println("a3: "+a3);
        
        long a4=18;
        int b4=5;
        //b4=b4*a4; //no compila, ingresa un long en un int, falta castear
        b4*=a4; //compila, no hay promocion, hay un casteo implicito , es lo mismo de arriba, 
        
        System.out.println("b4: "+b4);
        
        long a5=5;
        //long b5=a5=3; //forma de escribir ambigua
        long b5=(a5=3);
        System.out.println("a5: "+a5);
        System.out.println("b5: "+b5);
        
        int a6=10,b6=20,c6=30;
        System.out.println(a6<b6);
        System.out.println(a6<=b6);
        System.out.println(a6>=b6);
        System.out.println("xd: "+ (a6>c6));  //el resultado demla comparacion es un booleano
        // | es cuando lo que este a la izquierda y a la derecha se evaluaran ambos, pero cuando es || si a la izquierda a se obtiene la respuesta, la derecha ya no se evalua
        int y154;
        boolean a7= true ||(y154<4); //el y<4 no se esta evaluando
        System.out.println("qq: "+a7);
        
        Object o; //se declara la varibale
       // o = new Object(1,"string"); //se instancia la variable, se puede hacer en la misma linea
        o = new Object(); 
        if (o!=null&& o.hashCode() < 5){
        // do something
        }
        if (o!=null & o.hashCode() < 5){ // primero se hace el mayor o menor, luego el !=, luego el & , &&(&>&&)
        // do something
        }
        int a8=6;
        boolean b8=(a8>=6) || (++a8<=7);
        System.out.println("a8: "+a8);
        
        boolean b9= false;
        boolean a9= (b9=true);
        System.out.println("a9: "+a9);
        System.out.println("b9: "+b9);
        System.out.println("Equality operator o para los cuates operador de cualidad");
        File p1=new File("file.txt"); //estamos instanciando//cuando las lineas rojas subrayan na palabra es porq falta añadir una libreria
        File q1=new File("file.txt"); //p1 y q1 estan instanciando el mismo archivo, p1 y q1 son referencias
        File r1=p1;
        System.out.println("p1 == q1? "+(p1==q1));
        System.out.println("p1 == r1? "+(p1==r1)); //xd
    }
}
