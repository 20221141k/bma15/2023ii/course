/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jrodriguezb.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author User
 */
public class App {

    public static void main(String[] args) throws FileNotFoundException, IOException { // eso lo coloco automaticamente el IDE cuando puse el FileOutputStream 
        System.out.println("Byte Stream with Files!");
        //File names
        String name="ByteStream.txt"; // se puede colocar una ruta, con funcionoes
        File file =new File(name);
        //write
        FileOutputStream fileOutputStream = new FileOutputStream(file);
    fileOutputStream.write('J');
    fileOutputStream.write('A');
    fileOutputStream.write('R');
    fileOutputStream.write('E');
        fileOutputStream.write('D');
        fileOutputStream.write('\n');
        fileOutputStream.write('X');
        fileOutputStream.write('D');
        fileOutputStream.close();
        System.out.println("Location: "+file.getAbsolutePath());
// read a file

FileInputStream fileInputStream=new FileInputStream(file);
int decimal;
while ((decimal= fileInputStream.read())!=-1){ // saca cada caracter y lo guarda en decimal hasta que finalize
    // mientras en C el termino vacio es \0, aca es -1
    System.out.print((char)(decimal));
}name = "CharacterStream.txt";
file =new File(name);
//write
FileWriter fileWriter= new FileWriter(file);
fileWriter.write('D');
fileWriter.write('A');
fileWriter.write('V');
fileWriter.write('I');
fileWriter.write('D');
fileWriter.close();
        System.out.println("FileWriter Location: "+file.getAbsoluteFile());
        // read
       FileReader fileReader= new FileReader(file);
       while ((decimal=fileReader.read())!=-1){
           System.out.print((char)(decimal));
       }
        System.out.println("");
        System.out.println("List of files");
        String[] paths;
        file = new File("C:\\Disco D\\goku ssj4"); // el "."representa la ruta del proyecto
        paths=file.list(); // en lista los archivos que encuentra
        System.out.println(Arrays.toString(paths));
        System.out.println("Creating directories");
        System.out.println("Absolute path: "+file.getAbsolutePath());
        String directory="/Files/Binaries/Selected"; //creamos un directoriooooooooooooooooooo  
        String fullPath= file.getAbsolutePath()+directory; // creamos un fullpath en la raiz mas directorio
        file =new File(fullPath);
        if (file.mkdirs()){// encuentra directorios de varios niveles por el "s"
            System.out.println("Directories have been created");
        }else {
            System.out.println("Directories have alredy been created");
        }
}
}