/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.interfaces;

/**
 *
 * @author PROFESOR
 */
public abstract class Animal {
    private double weight;
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    public abstract String sound();  /// metodo abstracta porque esta en cursiva en el UML
    // cuando esta subrayada, es una static
    
}
