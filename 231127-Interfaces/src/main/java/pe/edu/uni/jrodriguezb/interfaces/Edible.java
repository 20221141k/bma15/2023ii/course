/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pe.edu.uni.jrodriguezb.interfaces;

/**
 *
 * @author PROFESOR
 */
public interface Edible { // INTERFAZ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
    // las lines puntedadas me apuntan un interfaz
    //una clase no puede heredar de una interfaz
    //una clase no puede heredar de dos clases
    // una clase implementa de otra clase
    // cuando una interfaz hereda de otra interfaz, entonces esta heredando, no implementando
    //interface1 hereda de otras interfaces
    //de interfaz1 si hereda de interfaz2 entonces solo uso interfaz1, pues hereda de otra interfaz
    //EN UNA INTERFAZ SIEMPRE HAY METODOS ABSTRACTOS, SOLO HAY FIRMAS
    // NO HAY CODIGO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//    public abstract String howToEat();
    public String howToEat();
// EL YA SABE QUE TODOS SON ABSTRACTOS , NO ES NECESARIO EL ABSTRACT
// algo que sea del circulo y no del triangulo, entonces como no hay nada particular lo extendemos del interfaz, y ya no de la clase padre pq la clase padre no tiene la firma    
            
}
