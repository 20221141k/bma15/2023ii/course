/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jrodriguezb.interfaces;

/**
 *
 * @author PROFESOR
 */
public class TestInterfaces {

    public static void main(String[] args) {
        System.out.println("TestInterfaces!");
        Object[] objects={new Orange(), new Apple(), new Chicken(), new Tiger()};//inicializando
        // en la posicion cero no es de tipo orange, en la posicion 1 no es de tipo apple, etc
        // se hace un cast para que entienda que tipo es
        //objeect es la clase general de todas, la papa
        for (Object object: objects){
            // object es el object a procesar, el object es su tipo
            //como detecto a los objetos que son de animal
            
            if (object instanceof Animal){ // asi sabemos si es que es un instanceof de animal
                System.out.println(((Animal) object).sound()); // casteamos que es animal, y al caseteo le ponemos sound
            }
            if(object instanceof Edible){ // es instancia de la interfaz
                System.out.println(((Edible)object).howToEat());
            }
        }
    
    }
}//en listas
//insntancia celeste
//varialbes verde
//"variable" hola=new "instancia"();
//SUBRAYADO ES NEGRITA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!