/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.interfaces;

/**
 *
 * @author PROFESOR
 */
public abstract class Fruit implements Edible{// implementamos de la interfaz, en herencia es extends, 
    //las implementaciones van en las clases conctretas
    /// nivel 1 clases abstractas
    //nivel 2 clases abstractas implementadas por la interfaz
    //nivel 3 clases concretas
}
