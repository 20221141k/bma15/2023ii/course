/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jrodriguezb.lists;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Jared Rodriguez <juan.rodriguez.b@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Lists!");
        int[] numbers={9,6,4,5,7,3,1,0,4,7,8,};
        System.out.println("numbers: "+Arrays.toString(numbers));
        List<Integer> list=new LinkedList<>();
        for (Integer number :numbers) {
            if(!list.contains(number)){
                list.add(number);
            }
        }
        System.out.println("list: "+list);
        Collections.sort(list); // ordena de menor a mayor por defecto
        System.out.println("ordenado: "+list);
        Collections.shuffle(list);//distribuye de forma aleatoria el conedio
        System.out.println("aleatorio: "+list);
        Collections.reverse(list);// lo pone en reversa
        System.out.println("reverse: "+list);
    }
}
