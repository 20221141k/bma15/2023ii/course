/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.jrodriguezb.loops;

/**
 *
 * @author User
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Loops 1!");

        for (int i = 0; i < 10; i++) {
            if (i == 9) {
                System.out.print(i);

            } else {
                System.out.print(i + ", ");
            }
        }
        System.out.println(" ");

        System.out.println("Loops 2!");
        for (int i = 10; i > 0; i--) {
            System.out.println("i: " + i);
        }
        System.out.println("Array simple impresion");
        /*System.out.println("Loops 3!");
        for (int i=0 ;; ){
            System.out.println("i: "+i);
        }*/ // no se puede poner int i; !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // A[5,3]
        //A(1,1), A(1,2), A(1,3)
        //A(2,1), A(2,2), A(2,3)
        //A(3,1), A(3,2), A(3,3)
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                System.out.print("(" + i + "," + j + ")\t");
            }
            System.out.println("");
        }
        System.out.println("While Loop 1");
        int i = 1;
        while (i <= 5) {
            System.out.println("i: " + i);
            i++;
        }
        System.out.println("While Loop 2");
        int j = 5;
        while (1 <= j) {
            System.out.println("j: " + j);
            j--;
        }
        /*      System.out.println("While Loop 3");
        int k = 1;
        while (true) {
            System.out.println("k: " + k);
            k++;

        }*/
        int a = 1;
        while (a <= 5) {
            int b = 1;
            while (b <= 3) {
                System.out.print("(" + a + "," + b + ")\t");
                b++;
            }
            System.out.println("");
            a++;
        }
        System.out.println("Do while Loop");
        int p = 1;
        do {
            int q = 1;
            do {
                System.out.print("(" + p + "," + q + ")\t");
                q++;
            } while (q <= 3);
            p++;
            System.out.println("");
        } while (p <= 5);
    }
}
