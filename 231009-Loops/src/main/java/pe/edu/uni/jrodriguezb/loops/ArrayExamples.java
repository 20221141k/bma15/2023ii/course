/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.jrodriguezb.loops;

/**
 *
 * @author User
 */
public class ArrayExamples {

    public static void main(String[] args) {
       // new ArrayExamples().linealSearchOne();
       // new ArrayExamples().linealSearchTwo();
        new ArrayExamples().orderArray();
    }
    private void orderArray(){
        System.out.println("Ordenar un array de manera ascendente");
        int[] numeros={9,5,7,3,8,6,4,0,2,1};
        for (int i=0;i<numeros.length-1;i++){
            
        int minIndex=i;
        int minValue=numeros[minIndex];
        boolean foundMinValue=false;
        for (int j=i+1;j<numeros.length;j++){
            if (minValue>numeros[j]){
                minValue=numeros[j];
                minIndex=j;
                foundMinValue=true;
            }
        }
        System.out.print("\nMínimo encontrado: "+foundMinValue+" valor: "+minValue+" en la posición "+minIndex+",nuevo array: ");
        //intercambiar el valor minimo con la primera posicion
       
        numeros[minIndex]=numeros[i];
        numeros[i]=minValue;
        //visualizar array
        for (int j=0;j<numeros.length;j++){
        System.out.print(numeros[j]+" ");
        }
        }
        /*
        minIndex=1;
        minValue=numeros[minIndex];
        for (int i=2;i<numeros.length;i++){
            if (minValue>numeros[i]){
                minValue=numeros[i];
                minIndex=i;
            }
        }
    System.out.println("\nMínimo encontrado: "+minValue+" en la posición "+minIndex);
numeros[minIndex]=numeros[1];
        numeros[1]=minValue;
        //visualizar array
        for (int i=0;i<numeros.length;i++){
        System.out.print(numeros[i]+" ");
        }*/
    }
    private void linealSearchTwo() {
        System.out.println("Búsqueda lineal de los elementos (keys) encontrados en el arreay");
        int[] array = {1, 4, 8, 5, 2, 9, 4, 8,5,7,11,2};
        int key = 8;
        boolean found = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                System.out.println("Número encontrado en la posición : " + i);
                found = true; // usar booleean como flag para no perder mucha memoriaaaaaaaa
            }
        }
//        System.out.print(array[i]+ " ");
        if (!found) {// aaasí se niega !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            System.out.println("No se ha encontrado nada");
        }
    }

    private void linealSearchOne() {
        System.out.println("Búsqueda lineal de un elemento (key) donde todos los elementos son diferentes");
        int[] array = {1, 4, 8, 5, 2, 9, 3, 2};
        int key = 9;
        int index = -1;
        boolean found = false;
        for (int i = 0; i < array.length; i++) {
//                System.out.print(array[i]+" ");
            if (array[i] == key) {
                index = i;
                found = true;
                break; //el break no lleva ()
            }

        }
        //escribimos la solucion
        if (found) {// checka esto para la pc!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            System.out.println("elemento encontrado en la posición: " + index);
        } else {
            System.out.println("No se encontro el elemento");
        }
    }

}
