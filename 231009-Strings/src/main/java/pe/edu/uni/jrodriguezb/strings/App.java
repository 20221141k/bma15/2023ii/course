/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.jrodriguezb.strings;

/**
 *
 * @author User
 */
public class App {

    public static void main(String[] args) {
        String cadena = "   Jared Rodriguez ";
        
        System.out.println(cadena);
        
        //Length = longitud   Lenght of string= Longitud de cadena
        System.out.println("length of string: " + cadena.length());
        
        // character location
        
        System.out.println(cadena.charAt(10)); // divide la cadena en celdas empezando de cero
        System.out.println(cadena.charAt(cadena.length() - 1));
        
        // convert
        
        System.out.println(cadena.toUpperCase()); //hace que sean mayusculas
        //split
        
        String[] partes = cadena.split("e"); //dividimos la cadena hasta encontrar un caracter ya definido
        //visualizamos lo que hizo el split
        //ese caracter donde cortamos desaparece
        for (int i = 0; i < partes.length; i++) { // en for no usar ()alcostado de las funciones string.
            System.out.println(partes[i]);
        }
        //trim
        String trim = cadena.trim(); //elimina los espacios en blanco a los costados de la cadena
        System.out.println("trim: "+trim);
        System.out.println("length of trim: "+trim.length()); //para imprimir si usar () al costado de las funciones .
        String cadena2= "     Hola mundo que hay wasaaaaaa   "; 
        String[] hola=cadena2.split("d");
        System.out.println(hola[0]);
        System.out.println("trimeamos: "+cadena2.trim().length());
    }
       
}
