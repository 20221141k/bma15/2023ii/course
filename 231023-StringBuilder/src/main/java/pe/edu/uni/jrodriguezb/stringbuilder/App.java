/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jrodriguezb.stringbuilder;

/**
 *
 * @author User
 */
public class App {

    public static void main(String[] args) {
        System.out.println("String Builder!");
        String alpha= "";// el string es un array de carcteres,son inmutables, desde su creacion no pueden ser modificados
        for (char character='a';character<='z';character++){
            alpha+=character;
        }
        System.out.println("alpha: "+alpha);
        //StringBuilder
        StringBuilder beta= new StringBuilder() ;///beta , la referencia, no cambia, es siempre la misma, mientras que alpha ha ido cambiando agregando objetos
        for (char character='a';character<='z';character++){
            beta.append(character); // estamos agregando las variables a beta
        }
        System.out.println("beta: "+beta);
        StringBuilder sb=new StringBuilder("star"); //inicia con star
        sb.append("+middle"); // se le agrega middle
        StringBuilder same=sb.append("+end"); // lo grabo en otra cadena
        //si en same hubiera puesto un new, hubiera creado un nuevo objeto
        //pero lo que hize es copiar la referencia del sb
        System.out.println("same: "+same);
        System.out.println("same: "+sb); //sb y same esta apuntando al mismo objeto, si hago modificaion en uno hago modificaion en otro
    StringBuilder a=new StringBuilder("abc");
    StringBuilder b=a.append("de"); // se igualan las referencias
    b= b.append("f").append("g"); //las operaciones en b son en a tambien ya que son el mismo objeto
        System.out.println("a: "+a);
        System.out.println("b: "+b); //imprimen igual 
        
        //constructs
        StringBuilder sb1=new StringBuilder();
        StringBuilder sb2=new StringBuilder("animal");
        StringBuilder sb3=new StringBuilder(10); //reservar un numero de posiciones para caracteres, en este caso 10
        //tamaño es la cantidad de caracters qu almomento que se analiza se determina
        //capacidad es los espacios que jaca hace disponible al objeto
        //el string nace con un tamaño ya que es inmutable, no se puede cambiar a diferencai del builder
        // el string nace con un tamaño y ese tamaño es la capacidad que tiene el string pq no puede cambiar
        // substring()
        StringBuilder sb4=new StringBuilder("animals");
        String sub=sb4.substring(0, 5);
        System.out.println("sub: "+sub);
        System.out.println(sb4.indexOf("a"));
        System.out.println(sb4.indexOf("al"));
         sub=sb4.substring(sb4.indexOf("a"),sb4.indexOf("al"));
         System.out.println("sub: "+sub);
         System.out.println("length: "+sb4.length());
         char character=sb4.charAt(6);
         System.out.println("character: "+character);
         StringBuilder sb5=new StringBuilder("Animals");
         sb5.insert(7, "-");
//a partir de la posicion 7, se inserta un menos
         sb5.insert(0, "-");
 sb5.insert(4, "-");
         System.out.println("sb5: "+sb5);
//delete
StringBuilder sb6=new StringBuilder("abcdef");

sb6.deleteCharAt(5);
         System.out.println("sb6: "+sb6);

sb6.delete(1, 3); //borra desde 1 hasta antes que 3, osea 2
         System.out.println("sb6: "+sb6);
    
    //reverse
    StringBuilder sb7=new StringBuilder("ABC");
    sb7.reverse();
        System.out.println("sb7: "+sb7); // presentada como string builder
    
        String s=sb7.toString();
        System.out.println("s: "+s); /// presentada como string
    }
}
