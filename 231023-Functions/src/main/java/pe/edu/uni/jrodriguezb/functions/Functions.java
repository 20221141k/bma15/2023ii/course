/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.jrodriguezb.functions;

import java.util.Arrays;

/**
 *
 * @author User
 */
public class Functions {

    public static void main(String[] args) {
        System.out.println("Functions!");
        Functions();
        Functions.Functions();
        Functions.Functions("Jared");
        Functions.Functions("Jared",18);
        String[] result={"Hello ","Jared ",", you"," are"," 18", " years", " old", " !!!"};
       // System.out.println(Functions.Functions(result)); para retornar un array, usamos una clase
        System.out.println(Arrays.toString(result)); //object[] es el padre de string, int, etc, todas las otras clases
        for(int i=0;i<result.length;i++){
            System.out.print(result[i]);
        }
        System.out.println("");
        for(String string:result){// otra forma de escibir una matriz !!!!!
            System.out.print(string);
        }
        System.out.println("");
        int suma=Functions(1,2,3,4,5);
        System.out.println("suma: "+suma);
    }
    public static void Functions(){   // puede llamarse Functions igual que la clase
        System.out.println("Inside the function!!!");
    }
    public static void Functions(String name){ /*Sobrecarga de funcion cuando dos funciones tienen la
                                                     misma firma pero diferentes parametros de entrada */
        System.out.println("Inside the function !!! "+ name+"!!!");
    }
   /* public static int Functions(String name){ //no funciona
        
    }*/
    public static void Functions(String name, int age){ 
//System.out.println("Hello "+name + " you are "+ age+ " years old!!!");// el int es promovido a string
        System.out.println("Hello "+ name +", you are "+ String.valueOf(age)+" years old!!!");//lo mismo, solo que de la forma correcta
   //se convierte el inte en string en medio de strings
    }
   // public static void Functions(String args){// ya esta declarado
    //}
    public static String[] Functions(String[] args){
        return args;
    }
    public static int Functions(int... numbers){ // parametros indefinidos
        int total=0;
        for (int number:numbers){
            total+=number;
            System.out.print(number+ "\t");
        }
        return total;
    } // en el comit el verde son los agregados y el azul son los archivos alterados
}
